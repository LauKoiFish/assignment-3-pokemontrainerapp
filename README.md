# Pokemon Trainer

## Description

This is a project created in Noroff Accelerate .NET Full Stack -course. The goal was to create a working frontend application by using Angular with TypeScript.
The end result is a pokemon trainer application, where the user can catch wild pokemon and view pokemon they've already caught.

## Install
  - Download project files
  - Open src/environment.ts file and add the following into environment object (without square brackets)
```
apiKey: [PROVIDED WITH MOODLE],
apiPokemon: [PROVIDED WITH MOODLE],
apiUsers: [PROVIDED WITH MOODLE]
```
  - Then run following commands in project root
```
npm install
ng serve
```
  - Go to 'http://localhost:4200'

## Usage

To start the app, simply follow install instructions or visit the following Heroku link [Pokemon Trainer](https://limitless-savannah-66214.herokuapp.com/)

You can log in with any username to start catching wild pokemon!

In Trainer Profile -section, you will be able to see all the pokemon you've caught. Here you can also release the pokemon back to the wild by clicking them.

## Contributors

Contributors for this project:
[Laura Koivuranta](https://gitlab.com/LauKoiFish) and [Leevi Peltonen](https://gitlab.com/leevi-peltonen)
