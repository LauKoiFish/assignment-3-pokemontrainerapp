// Modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http'
// Components
import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FormsModule } from '@angular/forms';
// Pages
import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon-catalogue.page';
import { TrainerProfilePage } from './pages/trainer-profile/trainer-profile.page';
import { WildPokemonListComponent } from './components/wild-pokemon-list/wild-pokemon-list.component';
import { WildPokemonListItemComponent } from './components/wild-pokemon-list-item/wild-pokemon-list-item.component';
import { CatchButtonComponent } from './components/catch-button/catch-button.component';
import { LogoutButtonComponent } from './components/logout-button/logout-button.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    LoginFormComponent,
    NavbarComponent,
    PokemonCataloguePage,
    TrainerProfilePage,
    WildPokemonListComponent,
    WildPokemonListItemComponent,
    CatchButtonComponent,
    LogoutButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
