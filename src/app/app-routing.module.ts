import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guards/auth.guard";
import { LoginPage } from "./pages/login/login.page";
import { PokemonCataloguePage } from "./pages/pokemon-catalogue/pokemon-catalogue.page";
import { TrainerProfilePage } from "./pages/trainer-profile/trainer-profile.page";

const routes: Routes = [   // defining routes for the app
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: LoginPage,
  },
  {
    path: 'pokemon',
    component: PokemonCataloguePage,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'trainer',
    component: TrainerProfilePage,
    canActivate: [ AuthGuard ]
  }
]

@NgModule({
  imports: [      // import a module
    RouterModule.forRoot(routes)
  ],
  exports: [      // expose a module and its' features 
    RouterModule
  ]
})
export class AppRoutingModule {

}