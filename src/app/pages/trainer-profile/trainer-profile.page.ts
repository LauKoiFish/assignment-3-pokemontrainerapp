import { Component, OnInit } from '@angular/core';
import { PokemonResult } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer-profile',
  templateUrl: './trainer-profile.page.html',
  styleUrls: ['./trainer-profile.page.css']
})
export class TrainerProfilePage implements OnInit {

  get user(): User | undefined {
    return this.userService.user
  }

  get pokemons(): PokemonResult[] {
    if(this.userService.user) {
      return this.userService.user.pokemon
    }
    return []
  }

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
  }
}
