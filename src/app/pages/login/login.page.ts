import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth.guard';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit{

  constructor(
    private readonly router: Router,
    private readonly authGuard: AuthGuard
    ) { }


  handleLogin(): void {
    this.router.navigateByUrl('/pokemon')
  }

  ngOnInit() {
    if (this.authGuard.isLoggedIn) {
      this.router.navigateByUrl('/pokemon')
    }
 }

}
