import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.css']
})
export class LogoutButtonComponent implements OnInit {


  handleLogout(): void {
    this.userService.user = undefined
    this.router.navigateByUrl('/login')
  }

  constructor(
    private userService: UserService,    
    private readonly router: Router
    ) { }

  ngOnInit(): void {
  }

}
