import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WildPokemonListItemComponent } from './wild-pokemon-list-item.component';

describe('WildPokemonListItemComponent', () => {
  let component: WildPokemonListItemComponent;
  let fixture: ComponentFixture<WildPokemonListItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WildPokemonListItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WildPokemonListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
