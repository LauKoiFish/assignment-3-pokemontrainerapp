import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonResult } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { CaughtPokemonService } from 'src/app/services/caught-pokemon.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-wild-pokemon-list-item',
  templateUrl: './wild-pokemon-list-item.component.html',
  styleUrls: ['./wild-pokemon-list-item.component.css']
})
export class WildPokemonListItemComponent implements OnInit {

  isCaught: boolean = false

  @Input() pokemon?: PokemonResult;

  constructor(
    private readonly router: Router,
    private userService: UserService,
    private readonly caughtPokemonService: CaughtPokemonService
  ) { }

  ngOnInit(): void {
    if(this.pokemon === undefined) throw new Error('cannot find pokemon')
    this.isCaught = this.userService.isCaught(this.pokemon.id)
  }


  onProfile(): boolean {
    if(this.router.url.endsWith('trainer')) return true
    else return false
  }


  catchPokemon(): void {


    if(this.pokemon === undefined) throw new Error('cannot find pokemon')
    this.caughtPokemonService.catchPokemon(this.pokemon.id)
      .subscribe({
        next: (user: User) => {
          if(this.pokemon === undefined) throw new Error('cannot find pokemon')
          this.isCaught = this.userService.isCaught(this.pokemon.id)
        },
        error: (error: HttpErrorResponse) => {
          console.log('ERROR', error.message)
        }
      })
  }
  releasePokemon(): void {
    if(this.pokemon === undefined) throw new Error('cannot find pokemon')
    this.userService.releasePokemon(this.pokemon.id)
    this.caughtPokemonService.releasePokemon(this.pokemon.id)
      .subscribe({
        next: (user: User) => {
          if(this.pokemon === undefined) throw new Error('cannot find pokemon')
          this.isCaught = this.userService.isCaught(this.pokemon.id)
        },
        error: (error: HttpErrorResponse) => {
          console.log('ERROR', error.message)
        }
      })
  }
}
