import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WildPokemonListComponent } from './wild-pokemon-list.component';

describe('WildPokemonListComponent', () => {
  let component: WildPokemonListComponent;
  let fixture: ComponentFixture<WildPokemonListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WildPokemonListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WildPokemonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
