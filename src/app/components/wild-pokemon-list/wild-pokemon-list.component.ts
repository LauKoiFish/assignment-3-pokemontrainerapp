import { Component, Input, OnInit } from '@angular/core';
import { PokemonResult } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-wild-pokemon-list',
  templateUrl: './wild-pokemon-list.component.html',
  styleUrls: ['./wild-pokemon-list.component.css']
})
export class WildPokemonListComponent implements OnInit {

  @Input() pokemons: PokemonResult[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
