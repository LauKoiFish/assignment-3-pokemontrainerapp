import { PokemonResult } from "./pokemon.model";

export interface User {
  id: number;
  username: string;
  pokemon: PokemonResult[];
}

