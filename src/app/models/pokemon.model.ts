export interface Pokemon {
  name: string;
  url: string;
}

export interface Data {
  count: number;
  next: string;
  previous: null | string;
  results: Pokemon[];
}

export interface PokemonResult {
  id: number;
  name: string;
  image: string;
}