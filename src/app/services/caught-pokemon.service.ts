import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PokemonResult } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { UserService } from './user.service';

const { apiKey, apiUsers } = environment;

@Injectable({
  providedIn: 'root'
})
export class CaughtPokemonService {

  private _loading: boolean = false

  get loading(): boolean {
    return this._loading
  }


  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly userService: UserService
   ) { }

  
    public catchPokemon(pokemonId: number): Observable<User> {
      if(!this.userService.user) throw new Error('catchPokemon: There is no user')

      const user: User = this.userService.user
      const pokemon: PokemonResult | undefined = this.pokemonService.pokemonById(pokemonId)

      if(!pokemon) throw new Error('catchPokemon: no pokemon with id ' + pokemonId)

      if (this.userService.isCaught(pokemonId)) {
          alert('You already caught this Pokemon!')
          throw new Error('You already caught this Pokemon!')
      }

      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': apiKey,
      })

      this._loading = true

      return this.http.patch<User>(`${apiUsers}/${user.id}`, {
        pokemon: [...user.pokemon, pokemon]
      },{
        headers
      })
      .pipe(
        tap((updatedUser: User) => {
          this.userService.user = updatedUser
        }),
        finalize(() => this._loading = false)
      )
    }

    public releasePokemon(pokemonId: number): Observable<User> {
      if(!this.userService.user) throw new Error('releasePokemon: There is no user')

      const user: User = this.userService.user
      const pokemon: PokemonResult | undefined = this.pokemonService.pokemonById(pokemonId)

      this._loading = true
      const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'x-api-key': apiKey,
      })
      const filteredPokemon = user.pokemon.filter(item => item !== pokemon)
      return this.http.patch<User>(`${apiUsers}/${user.id}`, {
        pokemon: [...filteredPokemon]
      },{
        headers
      })
      .pipe(
        tap((updatedUser: User) => {
          this.userService.user = updatedUser
        }),
        finalize(() => this._loading = false)
      )
    }
}
