import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { PokemonResult } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?: User

  public get user(): User | undefined {
    return this._user
  }

  public set user(user: User | undefined) {
    StorageUtil.storageSave<User>(StorageKeys.User, user!)
    this._user = user
  }


  constructor() { 
    this._user = StorageUtil.storageRead<User>(StorageKeys.User)
  }

  public isCaught(pokemonId: number): boolean {
    if(this._user) {
      return Boolean(this._user?.pokemon.find((pokemon: PokemonResult) => pokemon.id === pokemonId))
    }
    return false
  }
  // use at profile page
  public releasePokemon(pokemonId: number): void {
    if (this._user) {
      this._user.pokemon = this._user.pokemon.filter((pokemon: PokemonResult) => pokemon.id !== pokemonId )
      
    }
  }

}
