import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Data, Pokemon, PokemonResult } from '../models/pokemon.model';

const { apiPokemon, resourcePokemonImage } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;
  private _pokemonFinal: PokemonResult[] = [];

  // Converts Pokemon type (name, url) in to PokemonResult (id, name, imageURL)
  private handlePokemon(): void {
    this._pokemons.forEach((pokemon) => {
      let tempPokemon: PokemonResult = {id: 0, name: '', image: ''}
      tempPokemon.name = pokemon.name.charAt(0).toUpperCase() + pokemon.name.slice(1);
      const foundID = pokemon.url.match(/(\d+)(?!.*\d)/) || []
      tempPokemon.id= parseInt(foundID[0])
      tempPokemon.image = `${resourcePokemonImage}${tempPokemon.id}.png`
      this._pokemonFinal.push(tempPokemon)

    })
  }




  public pokemonById(id: number): PokemonResult | undefined {
    return this._pokemonFinal.find((pokemon: PokemonResult) => pokemon.id === id)
  }

  get pokemons(): PokemonResult[] {
    return this._pokemonFinal;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }
  
  public findAllPokemons(): void {

    if(this._pokemons.length > 0 || this.loading) {
      return
    }

    this._loading = true; 
    this.http.get<Data>(apiPokemon)
    .pipe(
      finalize(() => {
        this.handlePokemon()
        this._loading = false;
      })
    )

    .subscribe({
      next: (result) => {
        this._pokemons = result.results; //
      },
      error: (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    })
  }
}
